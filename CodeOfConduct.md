---
layout: page
title: "Code of Conduct"
permalink: /CodeOfConduct/
---

All members of the Ashoka Science Community agree to be bound by the SciComm code of conduct

## TL;DR
Be kind to each other.

All SciComm spaces are intended to be environments where people can get together to learn from and be inspired by each other about all things Science. SciComm aims to foster an atmosphere of trust, openness, candour and safety where all members respect the diversity of each other’s talents, abilities and experiences and value the input of others.

SciComm is a diverse community. Sometimes this means we need to work harder to ensure we’re creating an environment of trust and respect where all who come to participate feel comfortable and included.

We value your participation and appreciate your help in realising this goal.

## Be respectful

Respect yourself, and respect others. Be courteous to those around you. If someone indicates they don’t wish to be photographed (physically or via a screenshot), respect that wish. If someone indicates they would like to be left alone, let them be. Our online spaces and event venues may be shared with members of the public; please be considerate to all patrons of these locations.

## Be inclusive

Any public presentation or communication which is part of any event is subject to this code of conduct and thus may not contain:

* sexual or violent imagery;
* exclusionary language;
* insults or ad-hominem attacks.

Members are asked to avoid language which is not appropriate for an all-ages audience as much as possible.

If the subject matter of the conversation, discussion thread, or in case of a physical event a presentation, cannot be presented adequately without including language that could be considered offensive, consult the moderators before posting and provide adequate warnings at the start of the communication.

If members are unsure whether their material is suitable, they are encouraged to show it to the forum moderators before posting.

## Be aware

We ask everyone to be aware that we will not tolerate intimidation, harassment, or any abusive, discriminatory or derogatory behaviour by anyone at any online space or physical event.

Complaints can be made to the moderators by contacting the relevant email addresses. All complaints made to moderators will remain confidential and be taken seriously. The complaint will be treated appropriately with discretion. Should event organisers or moderators consider it appropriate, measures they may take include:

* the individuals may be told to apologise
* the individuals may be told to stop/modify their behaviour appropriately
* the individuals may be warned that enforcement action may be taken if the behaviour continues
* the individuals may be asked to immediately leave the venue and/or the online space(s)
* the incident may be reported to the appropriate authorities (with the consent of complainant)

## What does that mean for me?

All participants, including event attendees and speakers must not engage in any intimidation, harassment, or abusive or discriminatory behaviour.

Here are some examples of behaviours which are not appropriate:

* offensive verbal or written remarks related but not limited to gender, sex, sexual orientation, disability, physical appearance, body size, race or religion;
* sexual or violent images in public spaces (including presentation slides);
* deliberate intimidation;
* stalking or following;
* unwanted photography or recording;
* sustained disruption of talks or other events;
* intoxication at an event venue;
* inappropriate physical contact;
* unwelcome sexual attention;
* sexist, racist, or other exclusionary jokes;
* unwarranted exclusion from online spaces, meetups or related events based on attributes such as (but not limited to) age, gender, sex, sexual orientation, disability, physical appearance, body size, race, religion.

We want everyone to have a good time at our events.

## Questions?

If you’re not sure about anything you’ve just read please contact the moderators or the Physics Community.

_This CoC was originally published by Linux Australia for PyCon AU and is a modified version of that document, available to be re-used or modified under the terms of the [Creative Commons Attribution-ShareAlike 3.0 Australia licence](https://creativecommons.org/licenses/by-sa/3.0/au/), available from CreativeCommons.org._
