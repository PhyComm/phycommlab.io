---
layout: page
title: About
permalink: /about/
---

This community was conceived to create a space for everybody interested in Science to share, discuss, learn and grow together. SciComm is a volunteer-led effort that is not affiliated with Ashoka in any official capacity, apart from the fact that it was made by students of Ashoka for (but not limited to!) other Ashoka students. Over time, we hope to build a community space similar to various Free and Open Source Software (FOSS) communities such as [LinuxChix](https://www.linuxchix.org) and [various Python users' groups](https://wiki.python.org/moin/LocalUserGroups) which have made amazing, quality content and bring together a community dedicated to open science and discussion. Anyone is welcome to join regardless of their qualifications and field of study to discuss and share their knowledge and experiences, and ask and offer help with navigating their interests within Science and academia at large.

Please do remember that this is a community and not a club or an organization. As such, the community does not have a hierarchical structure. Everybody is welcome to contribute what they can however they can, and we greatly appreciate your contributions. The only distinction present is between the community moderators and the members, with the moderators being trusted members that have worked with the community over time. In short, it is, and will continue to be, a very flat administrative structure.


